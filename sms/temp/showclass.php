<?php 
    include '../functions.php';
    $conn = dtatabaseconnection();
    $sql = "SELECT * from class";
    $result = mysqli_query($conn, $sql);
?>
<div class="container" style="width:100%">        
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="teacher_list">
                    <h2 class="text-center">Student's Information</h2>
                    <hr />
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="danger">
                                <th>Id</th>
                                <th>Class Name</th>
                                <th>Class ID</th>
                                <th>More</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if(mysqli_num_rows($result)>0){
                                    while($row = mysqli_fetch_assoc($result)) {
                            ?>
                                            <tr class="success">
                                                <td><?php echo $row['id'];?></td>
                                                
                                                <td><?php echo"<a href='./showstudent1.php?id=".$row['classid']."'>";?><strong><?php echo $row['name'];?></strong></a></td>
                                                <td><?php echo $row['classid'];?></td>

                                             
                                                <td ><a href="<?php echo 'editeclass.php?id='.$row['id'];  ?>"><button>Edit/Delete</button></a></td>
                                            </tr>
                            <?php
                                }
                                }
                                else
                                    echo "0 Result found";
                            ?>
                         
                        </tbody>
                        </table>                
                </div>
            </div>
        </div>        
    </div>