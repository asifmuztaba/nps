<?php 
    include '../functions.php';
    $conn = dtatabaseconnection();
    $sql = "SELECT * from teacher_info";
    $result = mysqli_query($conn, $sql);
?>
<div class="container" style="width:100%">        
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="teacher_list">
                    <h2 class="text-center">Teacher's Information By Class</h2>
                    <hr />
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="danger">
                                <th>Id</th>
                                <th>Teacher Name</th>
                                <th>Subject</th>
<th>Phone No.</th>
                                <th>More</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if(mysqli_num_rows($result)>0){
                                    while($row = mysqli_fetch_assoc($result)) {
                            ?>
                                            <tr class="success">
                                                <td><?php echo $row['id'];?></td>
                                                
                                                <td><strong><?php echo $row['name'];?></strong></td>
                                                <td><?php echo $row['subject'];?></td>
<td><?php echo $row['phone'];?></td>
                                             
                                                <td ><a href="<?php echo 'editeteacher.php?id='.$row['id'];  ?>"><button>Edit/Delete</button></a></td>
                                            </tr>
                            <?php
                                }
                                }
                                else
                                    echo "0 Result found";
                            ?>
                         
                        </tbody>
                        </table>                
                </div>
            </div>
        </div>        
    </div>