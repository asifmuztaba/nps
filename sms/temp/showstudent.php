<?php 
    include '../functions.php';
    $conn = dtatabaseconnection();
    $class_id=$_GET["id"];

    $sql = "SELECT * from student_info where class='$class_id'";


    $result = mysqli_query($conn, $sql);
?>
<div class="container" style="width:100%">        
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="teacher_list">
                    <h2 class="text-center">Student's Information</h2>
                    <hr />
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="danger">
                                <th>Id</th>
                                <th>Name</th>
                                <th>Roll</th>
                                <th>Gardian's Name</th>
                                <th>Mobile No.</th>
                                <th>Class</th>
                                <th>Details</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if(mysqli_num_rows($result)>0){
                                    while($row = mysqli_fetch_assoc($result)) {
                            ?>
                                            <tr class="success">
                                                <td><?php echo $row['id'];?></td>
                                                
                                                <td><strong><?php echo $row['name'];?></strong></td>
                                                <td><?php echo $row['roll'];?></td>
                                                <td><?php echo $row['father_name'];?></td>
                                                <td><?php echo $row['mobile'];?></td>
                                                <td><?php echo $row['class'];?></td>
                                             
                                                <td ><a href="<?php echo 'edite.php?id='.$row['id'];  ?>"><button>Edit/Delete</button></a></td>
                                            </tr>
                            <?php
                                }
                                }
                                else
                                    echo "0 Result found";
                            ?>
                         
                        </tbody>
                        </table> 

                </div>
            </div>
        </div>        
    </div>
