
    <?php include "dashboardheader.php";?>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <div id="responsbox">
                <div class="container" style="width:100%">
    <div class="row">
      <div class="col-lg-12">
        <div class="loadingj">
          <img src="images/load.gif" alt="">
        </div>
          <h1 class="text-center text-muted">Add a Class</h1>
        <hr />
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success text-center">Informations</h3>
        <hr />
        <div class="col-md-6">
        <form  id="adclassinfo" action="" method = "post" >
          <table class="table table-striped">
            <tbody>
              <tr>
              <td><strong>Class Name </strong></td>
              <td><input name="name" type="text" class="form-control" placeholder="Name"></td>
              </tr>
              <tr>
              <td><strong>Class Id </strong></td>
              <td><input name="classid" type="text" class="form-control" placeholder="Class ID."></td>
              </tr>             
            </tbody>
          </table>
          <div class="submit_btn" style="text-align:center;margin-bottom:40px">
            <button id="showclass" class="btn btn-warning btn-lg">Show All classes</button>
            <button id="addtheacher" type="submit" class="btn btn-primary btn-lg">Submit</button>
        </div>
     </form>
      </div>
      <div class="col-md-6">
        

      </div>
    </div>
    </div>

    
  </div>
          </div>
        </div>
      </div>
    </div>
    <?php include "dashboardfooter.php"?>