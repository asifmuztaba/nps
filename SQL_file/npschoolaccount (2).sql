-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 12, 2015 at 03:29 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `npschoolaccount`
--

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `classid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`id`, `name`, `classid`) VALUES
(3, 'class 3', 3),
(4, 'Class 5', 5),
(5, 'Class 6', 6),
(6, 'Class 7', 7),
(7, 'class8', 8),
(8, 'class 9', 9),
(9, 'class 10', 10),
(10, 'Class 2', 2),
(11, 'Class 55', 55),
(12, 'Class 552', 222),
(13, 'Class 55333', 3333),
(14, 'Class 558888', 888),
(15, 'stander 1 bangla version', 12);

-- --------------------------------------------------------

--
-- Table structure for table `costcategory`
--

CREATE TABLE IF NOT EXISTS `costcategory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cname` text COLLATE utf8_unicode_ci NOT NULL,
  `c_id` text COLLATE utf8_unicode_ci NOT NULL,
  `ctype` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `costcategory`
--

INSERT INTO `costcategory` (`id`, `cname`, `c_id`, `ctype`, `created_at`, `updated_at`) VALUES
(1, 'asd', '', '0', '2015-08-26 12:54:18', '2015-08-26 12:54:18'),
(2, 'servic', '987', 'expenditure', '2015-08-26 12:54:54', '2015-08-26 12:54:54'),
(3, 'Bills', '123', 'revenue', '2015-08-27 11:37:00', '2015-08-27 11:37:00'),
(4, 'Stuff Salary', '1', 'expenditure', '2015-09-12 12:31:20', '2015-09-12 12:31:20'),
(5, 'Telecommunication', '2', 'expenditure', '2015-09-12 12:31:44', '2015-09-12 12:31:44'),
(6, 'Cafateria', '3', 'expenditure', '2015-09-12 12:32:14', '2015-09-12 12:32:14'),
(7, 'Tiffine', '4', 'expenditure', '2015-09-12 12:33:17', '2015-09-12 12:33:17');

-- --------------------------------------------------------

--
-- Table structure for table `costdetail`
--

CREATE TABLE IF NOT EXISTS `costdetail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `voucher` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` date NOT NULL DEFAULT '0000-00-00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `costdetail`
--

INSERT INTO `costdetail` (`id`, `type`, `amount`, `voucher`, `note`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'L', '456', 'asdasd', 'asdasd', NULL, '2015-08-05', '2015-06-30 20:25:03'),
(2, 'L', '123123', '123123', '123123', NULL, '2015-07-01', '2015-07-01 02:26:38'),
(3, 'L', '40000', '456', 'This is test....', NULL, '2015-07-02', '2015-07-02 16:01:48'),
(4, 'S', '7000', '4578', 'after changing column type', NULL, '2015-07-03', '2015-07-02 22:01:17'),
(5, 'L', '5000', '123', 'Flash added', NULL, '2015-07-03', '2015-07-02 23:39:56'),
(6, 'L', 'asdas', '5645465', 'Updated after redirect solve 2/3', NULL, '2015-07-03', '2015-07-02 23:41:33'),
(7, 'L', '234', '456', 'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', NULL, '2015-07-04', '2015-07-04 05:06:17'),
(8, 'Rent Expense', '456', '7', 'asdasdasd', NULL, '2015-07-04', '2015-07-04 08:21:28'),
(9, 'Stuff Salary', '6516', '', '', NULL, '2015-07-04', '2015-07-04 08:29:22'),
(10, 'Stuff Salary', '4000', '1', 'dsfgsdgdsfg', NULL, '2015-07-04', '2015-07-04 08:29:49'),
(11, 'Photocopy and Printing', '5664878', '12345', 'fdgfsdgfd', NULL, '2015-07-04', '2015-07-04 08:30:16'),
(12, 'Utencils', '6000', '12', 'fdgfdgfd', NULL, '2015-07-04', '2015-07-04 08:30:41'),
(13, 'Stuff Salary', '5000', '1254', 'hi', NULL, '2015-07-08', '2015-07-08 06:01:05'),
(14, 'News Paper and Books', '52121', '123', '123', NULL, '2015-07-08', '2015-07-08 06:01:29'),
(15, 'Others', '4000', '123', '152', NULL, '2015-08-04', '2015-08-04 09:30:31'),
(16, 'Stuff Salary', '8000', '256', 'masuma gegum', NULL, '2015-08-04', '2015-08-04 10:18:55');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_06_30_210005_create_candidate_table', 1),
('2015_07_01_021745_create_costdetail_table', 2),
('2015_07_01_084751_create_revenuedetail_table', 3),
('2015_08_25_183021_create_users_table', 4),
('2015_08_25_193647_rollback', 5),
('2015_08_25_193730_create_users_table', 6),
('2015_08_25_193816_create_users_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `npusers`
--

CREATE TABLE IF NOT EXISTS `npusers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `npusers`
--

INSERT INTO `npusers` (`id`, `fullname`, `username`, `password`, `phone`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'asdasd', 'asdasd', '$2y$10$lDw0KLdsS7EQ0BWJCMOWr.x8MMGzAp8GzZWeBESa3RwQJNX9hYvAm', 'asdasd', 'gSBeeFD4vTakJvhq59LGR3jvZwJHH7n7Tr1BknjB6GHC7nZUPNihyvWfJFW2', '2015-06-30 11:02:44', '2015-07-02 23:26:41'),
(2, 'asif', 'asif', '$2y$10$5/1rhJYg4RiAYEW62YkFAOOHPv0W.G1yHs2Yxij6nwtsUir9ER.jy', '01776884415', 'wdIXh5yoym3v6pmrK0gQYkbrWca9kmNFbcAWCbs7tcZ1Mt17qRXU4d3R3vx4', '2015-06-30 12:54:04', '2015-08-04 10:13:32'),
(3, 'sk', 'laali', '$2y$10$q3uVl7uCqJhYc.pdguqBruFPf4DGLi9KvbWc8WfyZcm/9at1ts00W', '123', NULL, '2015-06-30 18:54:51', '2015-06-30 18:54:51'),
(5, 'asdasd', 'asdas', '$2y$10$aGjvNHcYRHKImBlGdm1U5eeYguvIq21I8SiUTqQyy51ef3JH1MfKe', 'asdasd', NULL, '2015-07-01 02:56:05', '2015-07-01 02:56:05');

-- --------------------------------------------------------

--
-- Table structure for table `npuserssms`
--

CREATE TABLE IF NOT EXISTS `npuserssms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `npuserssms`
--

INSERT INTO `npuserssms` (`id`, `fullname`, `username`, `password`, `phone`, `remember_token`, `created_at`, `updated_at`) VALUES
(6, 'Asif', 'asif', '202cb962ac59075b964b07152d234b70', '123', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `revenuecategory`
--

CREATE TABLE IF NOT EXISTS `revenuecategory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `revenuedetail`
--

CREATE TABLE IF NOT EXISTS `revenuedetail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `voucher` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` date NOT NULL DEFAULT '0000-00-00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `revenuedetail`
--

INSERT INTO `revenuedetail` (`id`, `type`, `amount`, `voucher`, `note`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'L', '456', 'asdasd', 'asdasd', NULL, '2015-07-01', '2015-07-01 02:50:31'),
(2, 'S', '100', '1010', 'hjghjjh', NULL, '2015-07-01', '2015-07-01 02:55:29'),
(3, 'Tution Fee', '4000', '1', 'khkjllkjlk', NULL, '2015-07-04', '2015-07-04 08:32:56'),
(4, 'Form Sell', '5000', '1254', 'hi', NULL, '2015-07-08', '2015-07-08 06:04:22'),
(5, 'Uniform Sell', '5000', '456', '659848', NULL, '2015-08-04', '2015-08-04 10:12:51');

-- --------------------------------------------------------

--
-- Table structure for table `student_info`
--

CREATE TABLE IF NOT EXISTS `student_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `roll` text NOT NULL,
  `mobile` text NOT NULL,
  `class` text NOT NULL,
  `father_name` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_2` (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `student_info`
--

INSERT INTO `student_info` (`id`, `name`, `roll`, `mobile`, `class`, `father_name`) VALUES
(4, 'asif', '12', '123', '1', 'adfasdfasdfdasf'),
(5, 'asif', '45', '4', 'asdasd', ''),
(6, 'sadasd', '234', '54645', '5', 'sfgdsfgfd'),
(7, 'asif', '2', '123', '6', 'xzfdsf'),
(8, 'raju', '09', '01724324902', '5', 'xcd'),
(9, 'r', '01', '01', '2', '122'),
(10, 'rr', '02', '01721587011', '2', '55'),
(11, 'asif', '02', '01721587011', '10', 'adfasdfasdfdasf'),
(12, 'asdasd', '12', '123456', '12', 'assd'),
(13, 'Liton POdder', '101', '01675494074', '12', 'slkjglkjfg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
