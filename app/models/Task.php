<?php

class Task extends Eloquent {
	protected $table = 'costdetail';
	protected $fillable = array('type', 'amount','voucher','note');
	protected $guarded = array();

	public static $rules = array();
}
