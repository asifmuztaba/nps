<?php

class Bank extends Eloquent {
	protected $table = 'bank_report';
	protected $fillable = array('purpose', 'username','amount','type','voucher');
	protected $guarded = array();

	public static $rules = array();
}
