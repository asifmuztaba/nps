<?php

class Rtask extends Eloquent {
	protected $table = 'revenuedetail';
	protected $fillable = array('type', 'amount','voucher','note');
	protected $guarded = array();

	public static $rules = array();
}
