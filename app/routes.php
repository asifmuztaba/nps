<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|viewReport
*/

####################  Bank Section  ############################
Route::post('withdrawBank', array('uses' => 'BankController@withdraw'))->before('auth');
Route::get('withdrawBank', array('uses' => 'BankController@withdraw'))->before('auth');
Route::post('storeBankWithdraw', array('uses' => 'BankController@storewithdraw'))->before('auth');
Route::get('storeBankWithdraw', array('uses' => 'BankController@storewithdraw'))->before('auth');
Route::post('checkOutBank', array('uses' => 'BankController@checkOutBank'))->before('auth');
Route::get('checkOutBank', array('uses' => 'BankController@checkOutBank'))->before('auth');




####################  Bank Section End  ########################
Route::post('deleteCat', array('uses' => 'CategoryController@deleteCat'))->before('auth');
Route::get('deleteCat', array('uses' => 'CategoryController@deleteCat'))->before('auth');
Route::post('DelCat', array('uses' => 'CategoryController@DelCat'))->before('auth');
Route::get('DelCat', array('uses' => 'CategoryController@DelCat'))->before('auth');
Route::post('searchByCat', array('uses' => 'AccountController@searchByCat'))->before('auth');
Route::get('searchByCat', array('uses' => 'AccountController@searchByCat'))->before('auth');
Route::post('viewReportdateOut', array('uses' => 'AccountController@viewReportdateOut'))->before('auth');
Route::get('viewReportdateOut', array('uses' => 'AccountController@viewReportdateOut'))->before('auth');
Route::post('updateCat', array('uses' => 'CategoryController@update'))->before('auth');
Route::get('updateCat', array('uses' => 'CategoryController@update'))->before('auth');
Route::post('showCat', array('uses' => 'CategoryController@show'))->before('auth');
Route::get('showCat', array('uses' => 'CategoryController@show'))->before('auth');
Route::post('storeCat', array('uses' => 'CategoryController@store'))->before('auth');
Route::get('storeCat', array('uses' => 'CategoryController@store'))->before('auth');
Route::post('viewReportdate', array('uses' => 'AccountController@viewReportdate'))->before('auth');
Route::get('viewReportdate', array('uses' => 'AccountController@viewReportdate'))->before('auth');
Route::post('viewReportyear', array('uses' => 'AccountController@viewReportyear'))->before('auth');
Route::get('viewReportyear', array('uses' => 'AccountController@viewReportyear'))->before('auth');
Route::post('viewReportmonth', array('uses' => 'AccountController@viewReportmonth'))->before('auth');
Route::get('viewReportmonth', array('uses' => 'AccountController@viewReportmonth'))->before('auth');
Route::get('viewReport', array('uses' => 'AccountController@viewReport'))->before('auth');
Route::get('deletecostrev/{id}', array('uses' => 'AccountController@deletecostrev'))->before('auth');
Route::post('updatecostmrev/{id}', array('uses' => 'AccountController@updatecostmrev'));
Route::get('updatecostrev/{id}', array('uses' => 'AccountController@updatecostrev'))->before('auth');
Route::get('detailsrev/{id}', array('uses' => 'AccountController@detailsrev'))->before('auth');
Route::post('viewByYearrev', array('uses' => 'AccountController@viewByYearrev'))->before('auth');
Route::get('viewByYearrev', array('uses' => 'AccountController@viewByYearrev'))->before('auth');
Route::post('showmonthrev', array('uses' => 'AccountController@showmonthrev'))->before('auth');
Route::get('showmonthrev', array('uses' => 'AccountController@showmonthrev'))->before('auth');
Route::post('showrevenue', array('uses' => 'AccountController@showrevenue'))->before('auth');
Route::get('showrevenue', array('uses' => 'AccountController@showrevenue'))->before('auth');
Route::post('viewByYear', array('uses' => 'AccountController@viewByYear'))->before('auth');
Route::get('viewByYear', array('uses' => 'AccountController@viewByYear'))->before('auth');
Route::post('showmonth', array('uses' => 'AccountController@showmonth'))->before('auth');
Route::get('showmonth', array('uses' => 'AccountController@showmonth'))->before('auth');
Route::post('show', array('uses' => 'AccountController@show'))->before('auth');
Route::get('deletecost/{id}', array('uses' => 'AccountController@deletecost'))->before('auth');
Route::post('updatecostm/{id}', array('uses' => 'AccountController@updatecostm'));
Route::get('updatecost/{id}', array('uses' => 'AccountController@updatecost'))->before('auth');

Route::get('details/{id}', array('uses' => 'AccountController@details'))->before('auth');
Route::get('show', array('uses' => 'AccountController@show'))->before('auth');
Route::post('revenuestore', array('uses' => 'AccountController@revenuestore'))->before('auth');
Route::get('rbalance',array('uses' => 'AccountController@rbalance'))->before('auth');
Route::post('store', array('uses' => 'AccountController@store'));
Route::get('accountManagement',array('uses' => 'HomeController@accountManagement'))->before('auth');
Route::get('category',array('uses' => 'CategoryController@index'))->before('auth');
Route::get('balance',array('uses' => 'AccountController@index'))->before('auth');
Route::post('userlogin','SessionController@store');
Route::get('home', array('uses' => 'HomeController@home'))->before('auth');
Route::get('bank', array('uses' => 'BankController@index'))->before('auth');
Route::get('logout','SessionController@destroy');
Route::resource('balances', 'AccountController');
Route::resource('sessions', 'SessionController');
Route::resource('npDataBase', 'DatabaseController');

Route::get('/', function()
{
	return View::make('login');
});
Route::get('register', function()
{
  return View::make('register');
});
Route::post('reginsert', array('uses' => 'DatabaseController@store'));