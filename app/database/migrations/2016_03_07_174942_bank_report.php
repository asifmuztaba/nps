<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BankReport extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
			Schema::create('bank_report', function($table)
		    {
		        $table->increments('id');
		        $table->string('purpose', 32);
		        $table->string('username', 32);
		        $table->string('amount', 64);
		        $table->string('phone', 32);
		       	$table->string('remember_token', 100)->nullable();
				$table->timestamps();
		    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bank_report');
	}

}
