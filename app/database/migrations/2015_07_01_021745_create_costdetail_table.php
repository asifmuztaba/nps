<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCostdetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('costdetail', function(Blueprint $table) {
				$table->increments('id');
		        $table->string('type', 32);
		        $table->string('amount', 32);
		        $table->string('voucher', 64);
		        $table->string('note', 32);
		       	$table->string('remember_token', 100)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('costdetail');
	}

}
