<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('npusers', function($table)
		    {
		        $table->increments('id');
		        $table->string('fullname', 32);
		        $table->string('username', 32);
		        $table->string('password', 64);
		        $table->string('phone', 32);
		       	$table->string('remember_token', 100)->nullable();
				$table->timestamps();
		    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('npusers');
	}

}
