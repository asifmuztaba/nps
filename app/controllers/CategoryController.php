<?php

class CategoryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
$s= Auth::user('username');		
return View::make('CategoryManagement')
->with('s',$s);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}
	public function showall()
	{
		
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
			$cname=Input::get('cname');
			$c_id=Input::get('c_id');
			$ctype=Input::get('ctype');
 
$s= Auth::user('username');
if($ctype=='0')
{
Session::flash('message', 'Please Select a catagory'); 

return View::make('CategoryManagement')
->with('s',$s);
}
else{
   $is= cTask::create(array(
   		
   		'cname'=>$cname,
        'c_id'     =>$c_id,
        'ctype' => $ctype
        ));

   if ($is) {
   	      Session::flash('message', 'Category Saved!'); 

return View::make('CategoryManagement')
->with('s',$s);
}
else 
{
   	      Session::flash('message', 'Category Not Saved!'); 

return View::make('CategoryManagement')
->with('s',$s);
   	# code...
   }
   }

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
					$ctype=Input::get('ctype'); 
		$users=DB::select( DB::raw("SELECT id,cname,ctype,c_id FROM costcategory where ctype='$ctype'"));
//var_dump($users);
			     return View::make('categorydetails')
                ->with('users', $users);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
$s= Auth::user('username');
			$cname=Input::get('cname');
			$id=Input::get('id');
			$c_id=Input::get('c_id');
			$ctype=Input::get('ctype'); 
			$users=DB::table('costcategory')
            ->where('id', $id)
            ->update(array('cname' => $cname,
            				'c_id' => $c_id,
            				'ctype'=> $ctype));
			
			   if ($users) {
   	      Session::flash('message', 'Category Updated!'); 

return View::make('CategoryManagement')->with('s',$s);
   }
   else{
   	Session::flash('message', 'Category Has not Updated!');
   	return View::make('CategoryManagement')->with('s',$s); 
   }


	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteCat()
	{
$ditem=Input::get('ditem');
foreach($ditem as $id){
$users =DB::table('costcategory')->where('id', $id)->delete();
}
Session::flash('message', 'Category Deleted Successfully');
	$s= Auth::user('username');		
return View::make('CategoryManagement')
->with('s',$s);	
	}
	public function DelCat()
	{
					$ctype=Input::get('ctype'); 
		$users=DB::select( DB::raw("SELECT id,cname,ctype,c_id FROM costcategory where ctype='$ctype'"));
//var_dump($users);
			     return View::make('DelCat')
                ->with('users', $users);
	}


}
