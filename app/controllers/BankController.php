<?php

class BankController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
				date_default_timezone_set('Asia/Dhaka');
		$present_time=date('Y'-'m'-'d');
		$last_withdraw_query=DB::select( DB::raw("SELECT amount,date(created_at) as cdate FROM bank_report WHERE type='0' ORDER BY id DESC LIMIT 1;"));
		$last_deposit_query=DB::select( DB::raw("SELECT amount,date(created_at) as cdate FROM bank_report WHERE type='1' ORDER BY id DESC LIMIT 1;"));
		$total_deposits=DB::select( DB::raw("SELECT sum(amount) as amount FROM bank_report WHERE type='1'"));
		$total_withdraws=DB::select( DB::raw("SELECT sum(amount) as amount FROM bank_report WHERE type='0'"));
		$td=0.0;
		$tw=0.0;
		foreach ($total_deposits as $total_deposit) {
			$td=$td+$total_deposit->amount;
		}
		foreach ($total_withdraws as $total_withdraw) {
			$tw=$tw+$total_withdraw->amount;
		}
		foreach ($last_deposit_query as $last_deposit){
			$last_deposit_amount=$last_deposit->amount;
			$last_deposit_cdate=$last_deposit->cdate;
		}
		foreach( $last_withdraw_query as $last_withdraw){
			$last_withdraw_amount=$last_withdraw->amount;
			$last_withdraw_cdate=$last_withdraw->cdate;
		}
		$remaining_balance=$td-$tw;
		//var_dump($remaining_balance);
		return View::make('bank')
		->with('remaining_balance',$remaining_balance)
		->with('last_withdraw_amount',$last_withdraw_amount)
		->with('last_withdraw_cdate',$last_withdraw_cdate)
		->with('last_deposit_amount',$last_deposit_amount)
		->with('last_deposit_cdate',$last_deposit_cdate);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function withdraw()
	{
		$s= Auth::user('username');
		date_default_timezone_set('Asia/Dhaka');
		$present_time=date('Y'-'m'-'d');
		$last_withdraw_query=DB::select( DB::raw("SELECT amount FROM bank_report WHERE type='0' ORDER BY id DESC LIMIT 1;"));
		$last_deposit_query=DB::select( DB::raw("SELECT amount FROM bank_report WHERE type='1' ORDER BY id DESC LIMIT 1;"));
		var_dump($last_deposit_query);
		return View::make('bankWithdraw')->with('s',$s)->with('last_withdraw_query',$last_withdraw_query)->with('last_deposit_query',$last_deposit_query);
	}

	public function checkOutBank()
	{
				$fromdate=Input::get('fromdate');
        $todate=Input::get('todate');
         // var_dump($fromdate);
          //var_dump($todate);

		date_default_timezone_set('Asia/Dhaka');
		$present_time=date('Y'-'m'-'d');
		$last_withdraw_query=DB::select( DB::raw("SELECT amount,date(created_at) as cdate FROM bank_report WHERE type='0' ORDER BY id DESC LIMIT 1;"));
		$last_deposit_query=DB::select( DB::raw("SELECT amount,date(created_at) as cdate FROM bank_report WHERE type='1' ORDER BY id DESC LIMIT 1;"));
		$total_deposits=DB::select( DB::raw("SELECT sum(amount) as amount FROM bank_report WHERE type='1'"));
		$total_withdraws=DB::select( DB::raw("SELECT sum(amount) as amount FROM bank_report WHERE type='0'"));
		$td=0.0;
		$tw=0.0;
		foreach ($total_deposits as $total_deposit) {
			$td=$td+$total_deposit->amount;
		}
		foreach ($total_withdraws as $total_withdraw) {
			$tw=$tw+$total_withdraw->amount;
		}
		foreach ($last_deposit_query as $last_deposit){
			$last_deposit_amount=$last_deposit->amount;
			$last_deposit_cdate=$last_deposit->cdate;
		}
		foreach( $last_withdraw_query as $last_withdraw){
			$last_withdraw_amount=$last_withdraw->amount;
			$last_withdraw_cdate=$last_withdraw->cdate;
		}
		$remaining_balance=$td-$tw;

          if(!empty($fromdate) and !empty($todate)){
$users=DB::select( DB::raw("SELECT * FROM bank_report WHERE date(created_at) BETWEEN '$fromdate' and '$todate'"));
if ($users) {
		return View::make('checkOutBank')		
		->with('remaining_balance',$remaining_balance)
		->with('last_withdraw_amount',$last_withdraw_amount)
		->with('last_withdraw_cdate',$last_withdraw_cdate)
		->with('last_deposit_amount',$last_deposit_amount)
		->with('last_deposit_cdate',$last_deposit_cdate)
		->with('users',$users);

	}
}
else{
		return View::make('checkOutBank')		
		->with('remaining_balance',$remaining_balance)
		->with('last_withdraw_amount',$last_withdraw_amount)
		->with('last_withdraw_cdate',$last_withdraw_cdate)
		->with('last_deposit_amount',$last_deposit_amount)
		->with('last_deposit_cdate',$last_deposit_cdate);
}
}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function storewithdraw()
	{
			$type=Input::get('type');
            $amount=Input::get('amount');
             $purpose=Input::get('purpose');
            $voucher=Input::get('voucher');
            
            $s= Auth::user('username');
$username=$s->username;
   if(!empty($type) or !empty($amount) and !empty($purpose))  {
   $is= Bank::create(array(
   		'type'=>$type,
        'amount'     =>$amount ,
        'voucher' => $voucher,
        'purpose'=> $purpose,
        'username'=>$username));
   if ($is) {
   	 Session::flash('message', 'Bank Withdraw record saved!'); 
   	 return View::make('bankWithdraw')->with('s',$s);
   }
   else{
   	 Session::flash('message', 'Bank Withdraw record Has not saved! Try again'); 
   	 return View::make('bankWithdraw')->with('s',$s);
   }
}
else{
	   	 Session::flash('message', 'Please Fill All the Fields.'); 
   	 return View::make('bankWithdraw')->with('s',$s);
}
        
     
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
