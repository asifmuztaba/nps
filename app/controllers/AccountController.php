<?php

class AccountController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

         $categories = DB::table('costcategory')->where('ctype', 'expenditure')->lists('cname', 'cname');
        //$users=DB::select( DB::raw("SELECT * FROM costcategory") );

		return View::make('expenditure')
                ->with('categories', $categories);
        
		//return "asif";
	}
	public function rbalance()

	{
         $categories = DB::table('costcategory')->where('ctype', 'revenue')->lists('cname', 'cname');
		return View::make('revenue')
        ->with('categories', $categories);
		//return "Asif";
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function deletecost($id)
	{
		
		$users =DB::table('costdetail')->where('id', $id)->delete();
		if($users){
         Session::flash('message', 'Cost Deleted Successfully'); 
	        $categories = DB::table('costcategory')->where('ctype', 'expenditure')->lists('cname', 'cname');
         return View::make('searchByCat')
            ->with('categories', $categories);
		}
		else{
         Session::flash('message', 'Cost Does not Deleted'); 
	        $categories = DB::table('costcategory')->where('ctype', 'expenditure')->lists('cname', 'cname');
         return View::make('searchByCat')
            ->with('categories', $categories);
		}

	}
		public function deletecostrev($id)
	{
		
		$users =DB::table('revenuedetail')->where('id', $id)->delete();
		if($users){
         Session::flash('message', 'Cost Deleted Successfully'); 
	        $categories = DB::table('costcategory')->where('ctype', 'revenue')->lists('cname', 'cname');
         return View::make('revenue')
            ->with('categories', $categories);
		}
		else{
         Session::flash('message', 'Cost Have Not Deleted Successfully'); 
	        $categories = DB::table('costcategory')->where('ctype', 'revenue')->lists('cname', 'cname');
         return View::make('revenue')

            ->with('categories', $categories);
		}

	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		    $type=Input::get('type');
            $amount=Input::get('amount');
            $voucher=Input::get('voucher');
            $note=Input::get('note');

     
   $is= Task::create(array(
   		
   		'type'=>$type,
        'amount'     =>$amount ,
        'voucher' => $voucher,
        
        'note' => $note
    ));
   //var_dump($is);
 
            
            if($is){
                $categories = DB::table('costcategory')->lists('cname', 'cname');
         Session::flash('message', 'Cost Saved Successfully'); 

return View::make('expenditure')
             ->with('categories', $categories);
            }
            else{
         Session::flash('message', 'Cost Does Not Saved! Try Again'); 

return View::make('expenditure');
            }

}
		public function revenuestore()
	{
		    $type=Input::get('type');
            $amount=Input::get('amount');
            $voucher=Input::get('voucher');
            $note=Input::get('note');
           
   $is= Rtask::create(array(
   		
   		'type'=>$type,
        'amount'     =>$amount ,
        'voucher' => $voucher,
        
        'note' => $note
    ));
   //var_dump($is);
 
            
            if($is){
                $categories = DB::table('costcategory')->where('ctype', 'revenue')->lists('cname', 'cname');
      Session::flash('message', 'Revenue Saved!'); 

return View::make('revenue')
->with('categories', $categories);
            }
            else{
         Session::flash('message', 'Revenue Does not Save!'); 

return View::make('revenue');
            }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
			$fromdate=Input::get('fromdate');
           $todate=Input::get('todate');
         // var_dump($fromdate);
          //var_dump($todate);
          if(!empty($fromdate) and !empty($todate)){
$users=DB::select( DB::raw("SELECT * FROM costdetail WHERE date(created_at) BETWEEN '$fromdate' and '$todate'"));

//var_dump($users);

            return View::make('show')
                ->with('users', $users);
            }
            else{
            	$users="";
            return View::make('show')
                ->with('users', $users);
            }
	}
		public function showrevenue()
	{
			$fromdate=Input::get('fromdate');
           $todate=Input::get('todate');
         // var_dump($fromdate);
          //var_dump($todate);
          if(!empty($fromdate) and !empty($todate)){
$users=DB::select( DB::raw("SELECT * FROM  revenuedetail WHERE date(created_at) BETWEEN '$fromdate' and '$todate'"));

//var_dump($users);

            return View::make('showrevenue')
                ->with('users', $users);
            }
            else{
            	$users="";
            return View::make('showrevenue')
                ->with('users', $users);
            }
	}
	
	public function showmonth()
	{

		$fromdate=Input::get('fromdate');
		if(empty($fromdate)){
		 return View::make('showmonth');	
		}
			$fromdate1=(explode(" ",$fromdate));    // $todate=Input::get('todate');
        // var_dump($fromdate1);
         $y=$fromdate1[0];
         $y1=$fromdate1[1];
         //var_dump($y);
         
          //var_dump($todate);
          if(!empty($y1)){
$users=DB::select( DB::raw("SELECT sum(amount) as amount,created_at,id,type,voucher,note,updated_at FROM costdetail where MONTHNAME(created_at) = '$y' and YEAR(created_at)='$y1' GROUP BY created_at") );
//var_dump($users);
			if($users){

				//var_dump($users);

            return View::make('showmonth')
                ->with('users', $users);
            }
            else{
             $users="";
         Session::flash('message', 'Please Select Right Month and Year'); 
            return View::make('showmonth')
                ->with('users', $users);
            }
            }
            else{
            	$users="";
            	Session::flash('message', 'Please Select Right Month and Year'); 
            return View::make('showmonth')
                ->with('users', $users);
            }
	}
	public function showmonthrev()
	{

		$fromdate=Input::get('fromdate');
		if(empty($fromdate)){
		 return View::make('showmonthrev');	
		}
			$fromdate1=(explode(" ",$fromdate));    // $todate=Input::get('todate');
         //var_dump($fromdate1);
         $y=$fromdate1[0];
         $y1=$fromdate1[1];
         //var_dump($y);
         
          //var_dump($todate);
          if(!empty($y1)){
$users=DB::select( DB::raw("SELECT sum(amount) as amount,created_at FROM revenuedetail where MONTHNAME(created_at) = '$y' and YEAR(created_at)='$y1' GROUP BY created_at"));
//var_dump($users);
			if($users){

				//var_dump($users);

            return View::make('showmonthrev')
                ->with('users', $users);
            }
            else{
             $users="";
         Session::flash('message', 'Please Select Right Month and Year'); 
            return View::make('showmonthrev')
                ->with('users', $users);
            }
            }
            else{
            	$users="";
            	Session::flash('message', 'Please Select Right Month and Year'); 
            return View::make('showmonthrev')
                ->with('users', $users);
            }
	}
	
	public function details($id)
	{
        $s= Auth::user('username');
        
$users = DB::table('costdetail')->where('id', $id)->first();
//var_dump($users);
            return View::make('details')
                ->with('users', $users)
                ->with('s',$s);
	}
public function detailsrev($id)
	{
                $s= Auth::user('username');
    
$users = DB::table('revenuedetail')->where('id', $id)->first();
//var_dump($users);
            return View::make('detailsrev')
                ->with('users', $users)
                ->with('s',$s);
	}



	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updatecost($id)
	{
         $categories = DB::table('costcategory')->where('ctype', 'expenditure')->lists('cname', 'cname');	
$users = DB::table('costdetail')->where('id', $id)->first();
            return View::make('updatecostdetails')
            ->with('users', $users)
            ->with('categories', $categories);
                
	}
	public function updatecostrev($id)
	{
	         $categories = DB::table('costcategory')->where('ctype', 'revenue')->lists('cname', 'cname');
$users = DB::table('revenuedetail')->where('id', $id)->first();

            return View::make('updatecostdetailsrev')
            ->with('users', $users)
    ->with('categories', $categories);
                
	}
	public function updatecostm($id)
	{
			$type=Input::get('type');
            $amount=Input::get('amount');
            $voucher=Input::get('voucher');
            $note=Input::get('note');
	
$users = DB::table('costdetail')
            ->where('id', $id)
            ->update(array(
   		
   		'type'=>$type,
        'amount'     =>$amount ,
        'voucher' => $voucher,
        
        'note' => $note));
            
if($users){
	    Session::flash('message', 'Cost Details Updated !'); 

return View::make('expenditure');
}
else{
	return "asdhakjsd";
}
 
                
	}
	public function updatecostmrev($id)
	{
			$type=Input::get('type');
            $amount=Input::get('amount');
            $voucher=Input::get('voucher');
            $note=Input::get('note');
	
$users = DB::table('revenuedetail')
            ->where('id', $id)
            ->update(array(
   		
   		'type'=>$type,
        'amount'     =>$amount ,
        'voucher' => $voucher,
        
        'note' => $note));
            
if($users){
	    Session::flash('message', 'Cost Details Updated !'); 

return View::make('revenue');
}
else{
	return "asdhakjsd";
}
 
                
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}
	public function viewByYear()
	{
		$fromdate=Input::get('fromdate');
			$fromdate1=(explode(" ",$fromdate));    // $todate=Input::get('todate');
        // var_dump($fromdate1);
         $y=$fromdate1[0];
         //var_dump($y);
         
          //var_dump($todate);
          if(!empty($y)){
$users=DB::select( DB::raw("SELECT sum(amount) as amount,MONTHNAME(created_at) as month FROM costdetail where YEAR(created_at)='$y' GROUP BY MONTHNAME(created_at)") );
//var_dump($users);
			if($users){

				//var_dump($users);

            return View::make('viewByYear')
                ->with('users', $users);
            }
            else{
             $users="";
             Session::flash('message', 'Please Select Right Year'); 
            return View::make('viewByYear')
                ->with('users', $users);
            }
            }
            else{
            	$users="";
           Session::flash('message', 'Please Select Right First'); 
            return View::make('viewByYear')
                ->with('users', $users);
            }
	}
public function viewByYearrev()
	{
		$fromdate=Input::get('fromdate');
			$fromdate1=(explode(" ",$fromdate));    // $todate=Input::get('todate');
        // var_dump($fromdate1);
         $y=$fromdate1[0];
         //var_dump($y);
         
          //var_dump($todate);
          if(!empty($y)){
$users=DB::select( DB::raw("SELECT sum(amount) as amount,MONTHNAME(created_at) as month FROM revenuedetail where YEAR(created_at)='$y' GROUP BY MONTHNAME(created_at)") );
//var_dump($users);
			if($users){

				//var_dump($users);

            return View::make('viewByYearrev')
                ->with('users', $users);
            }
            else{
             $users="";
             Session::flash('message', 'Please Select Right Year'); 
            return View::make('viewByYearrev')
                ->with('users', $users);
            }
            }
            else{
            	$users="";
           Session::flash('message', 'Please Select Right First'); 
            return View::make('viewByYearrev')
                ->with('users', $users);
            }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//viewReportmonth
	}
    public function viewReport()
    {
                   return View::make('viewReport');
    }
    public function viewReportmonth()
    {
            $fromdate=Input::get('fromdate');
        if(empty($fromdate)){
         return View::make('viewReportmonth');    
        }
            $fromdate1=(explode(" ",$fromdate));    // $todate=Input::get('todate');
         //var_dump($fromdate1);
         $y=$fromdate1[0];
         $y1=$fromdate1[1];
         //var_dump($y);
         
          //var_dump($todate);
          if(!empty($y1)){
$users=DB::select( DB::raw("SELECT sum(amount) as amount,MONTHNAME(created_at)as month,YEAR(created_at) as year  FROM costdetail where MONTHNAME(created_at) = '$y' and YEAR(created_at)='$y1' GROUP BY created_at") );
$users1=DB::select( DB::raw("SELECT sum(amount) as amount,MONTHNAME(created_at) as month FROM revenuedetail where MONTHNAME(created_at) = '$y' and YEAR(created_at)='$y1' GROUP BY created_at") );
//var_dump($users);
            if($users or $users1){

                //var_dump($users);
                //var_dump($users1);

            return View::make('viewReportmonth',compact('users', 'users1'));
            }
            else{
             $users="";
         Session::flash('message', 'Please Select Right Month and Year'); 
            return View::make('viewReportmonth')
                ->with('users', $users);
            }
            }
            else{
                $users="";
                Session::flash('message', 'Please Select Right Month and Year'); 
            return View::make('viewReportmonth')
                ->with('users', $users);
            }
    }
        public function viewReportyear()
    {
            $fromdate=Input::get('fromdate');
        if(empty($fromdate)){
         return View::make('viewReportyear');    
        }
           $fromdate=Input::get('fromdate');
            $fromdate1=(explode(" ",$fromdate));    // $todate=Input::get('todate');
        // var_dump($fromdate1);
         $y=$fromdate1[0];
         //var_dump($y);
         
          //var_dump($todate);
          if(!empty($y)){
$users=DB::select( DB::raw("SELECT sum(amount) as amount,YEAR(created_at) as year FROM costdetail where YEAR(created_at)='$y' GROUP BY MONTHNAME(created_at)") );
$users1=DB::select( DB::raw("SELECT sum(amount) as amount,YEAR(created_at) as year FROM revenuedetail where YEAR(created_at)='$y' GROUP BY MONTHNAME(created_at)") );
//var_dump($users);
//var_dump($users);
            if($users and $users1){

                //var_dump($users);
                //var_dump($users1);

            return View::make('viewReportyear',compact('users', 'users1'));
            }
            else{
             $users="";
         Session::flash('message', 'Please Select Right Month and Year'); 
            return View::make('viewReportyear')
                ->with('users', $users);
            }
            }
            else{
                $users="";
                Session::flash('message', 'Please Select Right Month and Year'); 
            return View::make('viewReportyear')
                ->with('users', $users);
            }
    }
            public function viewReportdate()
    {
            $fromdate=Input::get('fromdate');
        if(empty($fromdate)){
         return View::make('viewReportdate');    
        }
           $fromdate=Input::get('fromdate');
            $fromdate1=(explode(" ",$fromdate));    // $todate=Input::get('todate');
        // var_dump($fromdate1);
         $y=$fromdate1[0];
         //var_dump($y);
         
          //var_dump($todate);
          if(!empty($y)){
$users=DB::select( DB::raw("SELECT sum(amount) as amount,created_at FROM costdetail where created_at='$y' GROUP BY created_at") );
$users1=DB::select( DB::raw("SELECT sum(amount) as amount,created_at FROM revenuedetail where created_at='$y' GROUP BY created_at") );
//var_dump($users);
//var_dump($users);
            if($users or $users1){

              // var_dump($users);
              // var_dump($users1);

            return View::make('viewReportdate',compact('users', 'users1'));
            }
            else{
             $users="";
         Session::flash('message', 'Please Select Right Month and Year'); 
            return View::make('viewReportdate')
                ->with('users', $users);
            }
            }
            else{
                $users="";
                Session::flash('message', 'Please Select Right Month and Year'); 
            return View::make('viewReportdate')
                ->with('users', $users);
            }
    }
    public function viewReportdateOut(){
        $fromdate=Input::get('fromdate');
        if(empty($fromdate)){
         return View::make('viewReportdateout1');    
        }
           $fromdate=Input::get('fromdate');
            $fromdate1=(explode(" ",$fromdate));    // $todate=Input::get('todate');
        // var_dump($fromdate1);
         $y=$fromdate1[0];
         //var_dump($y);
         
          //var_dump($todate);
          if(!empty($y)){
    $users=DB::select( DB::raw("SELECT costcategory.cname,costcategory.c_id,sum(costdetail.amount) as amount,costdetail.voucher FROM costcategory INNER JOIN costdetail on costdetail.type=costcategory.cname WHERE costdetail.created_at='$y' GROUP BY costdetail.type ORDER BY costcategory.id ASC") ); 
    $users1=DB::select( DB::raw("SELECT costcategory.cname,costcategory.c_id,sum(revenuedetail.amount) as amount,revenuedetail.voucher FROM costcategory INNER JOIN revenuedetail on revenuedetail.type=costcategory.cname WHERE revenuedetail.created_at='$y' GROUP BY revenuedetail.type ORDER BY costcategory.id ASC  ") ); 
       //var_dump($users);
      // var_dump($users1);
        



        return View::make('dailypayment')
           ->with('users', $users)
           ->with('users1', $users1);

    }
}
    public function searchByCat(){
        $catname=Input::get('type');
        $fromdate=Input::get('fromdate');
        $todate=Input::get('todate');
        if(empty($catname)){
        $categories = DB::table('costcategory')->where('ctype', 'expenditure')->lists('cname', 'cname');
         return View::make('searchByCat')
            ->with('categories', $categories);   
        }
else{
$users=DB::select( DB::raw("SELECT id,type,sum(amount) as amount,voucher,note,created_at,updated_at FROM costdetail WHERE type='$catname' and date(created_at) BETWEEN '$fromdate' and '$todate' GROUP BY created_at"));
//var_dump($users);
          if(!empty($users)){
        $categories = DB::table('costcategory')->where('ctype', 'expenditure')->lists('cname', 'cname');
         return View::make('searchByCat')
            ->with('categories', $categories)
            ->with('users', $users);  
          }
else{
$categories = DB::table('costcategory')->where('ctype', 'expenditure')->lists('cname', 'cname');
Session::flash('message', 'Data Does not exist'); 
         return View::make('searchByCat')
            ->with('categories', $categories);  
}
    }
}
}