<?php

class DatabaseController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

            $username=Input::get('username');
            $pass=Input::get('password');
            $name=Input::get('name');
            $phone=Input::get('phone');
           
   $is= User::create(array(
   		
   		'fullname'=>$name,
        'username'     =>$username ,
        'password' => Hash::make($pass),
        
        'phone' => $phone
    ));
  // var_dump($is);
 
            
            if($is){
           Session::flash('message', 'Registered Successfully'); 
            return View::make('login');
            }
            else{
                      Session::flash('message', 'Something went wrong!! Check Again'); 
            return View::make('register');
            }
         

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
