@extends('master')

@section('content')
@include('innerMenucommands')
<div class="showtable" id="responsbox">
  <div style="width:300px;margin:0 auto;"class="showInput">
        <p>Select a category:</p>
                {{Form::open(array('url' => 'showCat', 'method' => 'post')) }}
              {{Form::label('fromdate','Enter Category Type:')}}
              {{ Form::select('ctype', array(
                                  'Slect A Type',
                                  'revenue'=>'Revenue Area',
                                  'expenditure'=>'Expenditure Area'
                                  ),'2',['class' => 'btn']) }}
                                 </br>
              {{Form::submit('Submit', array('class' => 'btn btn-primary btn-action-u'))}}
              {{ Form::close() }}
  </div>

  @if (!empty($users))
         <span style="display:none">{{$total=0;}}</span>
<table id="paginationNps" class="table table-bordered table-hover">
    <h3 style="width:100%; text-align:center; margin:10px;">Cost Report By Year:</h3>
    <thead>
      <tr class="danger">
        <th>Sl No.</th>
      	<th>Category Name</th>

        <th>Category ID</th>
        <th>Category Type</th>
        <th>Action</th>

      </tr>
    </thead>
    <tbody>
    	@foreach ($users as $property)
               <span style="display:none"></span>
      <tr class="success">
        <td>{{ $property->id }}</td>
<td>{{ $property->cname }}</td>
<td>{{ $property->c_id }}</td>
        <td>{{ $property->ctype}}</td>
<td><button class="btn btn-primary" type="button" data-toggle="modal" data-target="#my{{ $property->id }}">Edite</button></td>
      </tr>
      <!-- Modal -->
<div id="my{{ $property->id }}" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update A product Category</h4>
      </div>
      <div class="modal-body">
          <div style="width:300px;margin:0 auto;margin-bottom:15px"class="cat">
              {{Form::open(array('url' => 'updateCat', 'method' => 'post')) }}
              {{Form::label('fromdate','Enter Category Name:')}}
              {{Form::text('cname', $property->cname,array('class' => 'form-control'))}}
              {{Form::text('id', $property->id,array('class' => 'form-control hidden'))}}
              {{Form::label('fromdate','Enter Category ID:')}}
              {{Form::text('c_id', $property->c_id,array('class' => 'form-control'))}}
              {{Form::label('fromdate','Enter Category Type:')}}
              {{ Form::select('ctype', array(
                                  'Slect A Type',
                                  'revenue'=>'Revenue Area',
                                  'expenditure'=>'Expenditure Area'
                                  ),'2',['class' => 'btn btn-success']) }}
                                 </br>
              {{Form::submit('Submit', array('class' => 'btn btn-primary'))}}
              {{ Form::close() }}
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
      @endforeach
    </tbody>
  </table>

<button class="pbtn" onclick="window.print();">Print Content</button>
<span style="border-top:1px solid; float:right;margin-top:40px;">Athaurized Signature</span>
  @else
  <br>
  <div class="alert alert-warning fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please Insert The Date Range.</strong></div>
  @endif
</div>
@stop
