@extends('master')

@section('content')

@include('innerMenu')
<div class="row acc_div">
	<div class="col-lg-6 acc_left">
		<div class="">
			<h2>Add Category</h2>
				<div class="account_sce">
				  <div style="width:300px;margin:0 auto;margin-bottom:15px"class="cat">
					    {{Form::open(array('url' => 'storeCat', 'method' => 'post')) }}
					    {{Form::label('fromdate','Enter Category Name:')}}
					    {{Form::text('cname', null,array('class' => 'form-control','required' => 'required'))}}
					    {{Form::label('fromdate','Enter Category ID:')}}
					    {{Form::text('c_id', null,array('class' => 'form-control','required' => 'required'))}}
					    {{Form::label('fromdate','Enter Category Type:')}}
					    {{ Form::select('ctype', array('Slect A Type','revenue'=>'Revenue Area','expenditure'=>'Expenditure Area'),'2',['class' => 'btn btn-success']) }}
															   </br>
					    {{Form::submit('Submit', array('class' => 'btn btn-primary'))}}
					    {{ Form::close() }}
					</div>
				</div>			
		</div>
	</div>
	<div class="col-lg-6 acc_right">
		<div class="">
			<h2>Action</h2>
  @if($s->username=='npadmin')
			<button class="btn btn-primary btn-action-u" type="button" data-toggle="modal" data-target="#myModal">Update</button><br>
			<button class="btn btn-danger btn-action-d" type="button" data-toggle="modal" data-target="#myModal1">Delete</button>
  @else
			<button disabled="disable" class="btn btn-primary btn-action-u" type="button" data-toggle="modal" data-target="#myModal">Update</button><br>
			<button disabled="disable" class="btn btn-danger btn-action-d" type="button" data-toggle="modal" data-target="#myModal1">Delete</button>
@endif
		</div>
	</div>	
</div>


@stop
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update A product Category</h4>
      </div>
      <div class="modal-body">
        <p>Select a category:</p>
        				{{Form::open(array('url' => 'showCat', 'method' => 'post')) }}
					    {{Form::label('fromdate','Enter Category Type:')}}
					    {{ Form::select('ctype', array(
															    'Slect A Type',
															    'revenue'=>'Revenue Area',
															    'expenditure'=>'Expenditure Area'
															    ),'2',['class' => 'btn']) }}
															   </br>
					    {{Form::submit('Submit', array('class' => 'btn btn-primary btn-action-u'))}}
					    {{ Form::close() }}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete product Category</h4>
      </div>
      <div class="modal-body">
      <p>Select a category:</p>
        				{{Form::open(array('url' => 'DelCat', 'method' => 'post')) }}
					    {{Form::label('fromdate','Enter Category Type:')}}
					    {{ Form::select('ctype', array(
															    'Slect A Type',
															    'revenue'=>'Revenue Area',
															    'expenditure'=>'Expenditure Area'
															    ),'2',['class' => 'btn']) }}
															   </br>
					    {{Form::submit('Submit', array('class' => 'btn btn-primary btn-action-u'))}}
					    {{ Form::close() }}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>