@extends('master')

@section('content')
@include('innerMenucommands')
<div class="showtable" id="responsbox">
  <div class="row">
  <div class="detailsbox col-lg-9 col-md-9 col-sm-9">
<table id="paginationNps" class="table table-bordered table-hover">
    <tr>
        <th style="width:200px;"class="danger">Cost Date</th>
        <td class="success">{{ $users->created_at }}</td>
      </tr>
      <tr>
        <th style="width:200px;"class="danger">Last Update Date</th>
        <td class="success">{{ $users->updated_at }}</td>
      </tr>
      <tr>
        <th style="width:200px;" class="danger">Id No.</th>
        <td class="success">{{ $users->id }}</td>
      </tr>
      <tr>
        <th  style="width:200px;" class="danger">Particularies of Cost</th>
        <td class="success">{{ $users->type }}</td>
      </tr>
      <tr>
        <th style="width:200px;" class="danger">Amount(in Taka)</th>
        <td class="success">{{ $users->amount }}</td>
      </tr>
      <tr>
        <th style="width:200px;" class="danger">Voucher No.</th>
        <td class="success">{{ $users->voucher }}</td>
      </tr>
      <tr>
        <th style="width:200px;" class="danger">Note</th>
        <td class="success">{{ $users->note }}</td>
      </tr>
      <tr> 
  </table>
  </div>
  @if($s->username=='npadmin')
  <div style="text-align:center;"class="actionbox col-lg-3 col-md-3 col-sm-3">
    <a href="{{ URL::to('updatecost',$users->id) }}"><button class="btn btn-primary">Update</button></a>
    <a href="{{ URL::to('deletecost',$users->id) }}"><button class="btn btn-danger">Delete</button></a>
  </div>
  @else
    <div style="text-align:center;"class="actionbox col-lg-3 col-md-3 col-sm-3">
    <a href="{{ URL::to('updatecost',$users->id) }}"><button disabled="disable" class="btn btn-primary">Update</button></a>
    <a href="{{ URL::to('deletecost',$users->id) }}"><button disabled="disable" class="btn btn-danger">Delete</button></a>
  </div>
  @endif
  </div>
</div>

@stop