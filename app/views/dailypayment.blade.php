<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Daily Received and Payment Sheet</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/pay.css" rel="stylesheet" media="all">
    <link href="css/payprint.css" rel="stylesheet" media="print">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container main_padd">
		<div class="row">
			<div class="col-lg-12">
				<div class="header_section">
					<h3>North Point School</h3>
					<h4>BGC Complex</h4>
					<h4>Daily Receved and Payment Sheet</h4>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="date_section">
					<h5>Date: <input type="text" name=""/></h5>
				</div>
			</div>
		</div>		
		<div class="row">
			<div class="col-lg-6 rece_table">
				<div class="main_table_section">
					<div class="table-responsive">
						<table class="table table-bordered payment_table" style="cells">
							<thead>
								<tr>
									<th colspan="4">Received</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Code</td>
									<td class="paticle">Particulars</td>
									<td class="voucher">Voucher No</td>
									<td class="amount">Amount (Taka)</td>
								</tr>
								@if(!empty($users1))
								{{-- */$t=0;/* --}}
								{{-- */$x=0;/* --}}
								@foreach($users1 as $property1)							
								<tr>
									<td><input class="code_in" type="text" name="" value="{{$property1->c_id}}" /></td>
									<td><input class="paticle_in" type="text" name="" value="{{$property1->cname}}" /></td>
									<td><input class="voucher_in" type="text" name="" value="{{$property1->voucher}}"/></td>
									<td><input class="amount_in" type="text" name="" value="{{$property1->amount}}"/></td>
								</tr>
								{{-- */$x=$property1->amount;/* --}}
								{{-- */$t=$t+$x;/* --}}
								@endforeach
								<tr>
									<td>Total</td>
									<td><input class="voucher_in" type="text" name=""/></td>
									<td><input class="amount_in" type="text" name="" /></td>	
									<td><input class="code_in" type="text" name="" value="{{$t}}"/></td>								
								
								</tr>
								@else
								<tr>
									<td><input class="code_in" type="text" name="" value="" /></td>
									<td><input class="paticle_in" type="text" name="" value="" /></td>
									<td><input class="voucher_in" type="text" name="" value=""/></td>
									<td><input class="amount_in" type="text" name="" value=""/></td>
								</tr>
								

	
								@endif						
							</tbody>
						</table>
					</div>				
				</div>
			</div>
			<div class="col-lg-6 pay_table">
				<div class="main_table_section">
					<div class="table-responsive">
						<table class="table table-bordered payment_table" style="cells">
							<thead>
								<tr>
									<th colspan="4">Payment</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Code</td>
									<td class="paticle">Particulars</td>
									<td class="voucher">Voucher No</td>
									<td class="amount">Amount (Taka)</td>
								</tr>
								@if(!empty($users))
								{{-- */$t=0;/* --}}
								{{-- */$x=0;/* --}}
								@foreach($users as $property)							
								<tr>
									<td><input class="code_in" type="text" name="" value="{{$property->c_id}}" /></td>
									<td><input class="paticle_in" type="text" name="" value="{{$property->cname}}"/></td>
									<td><input class="voucher_in" type="text" name="" value="{{$property->voucher}}"/></td>
									<td><input class="amount_in" type="text" name="" value="{{$property->amount}}" /></td>
								</tr>
								{{-- */$x=$property->amount;/* --}}
								{{-- */$t=$t+$x;/* --}}
								@endforeach
								<tr>
									<td>Total</td>
									<td><input class="voucher_in" type="text" name=""/></td>
									<td><input class="amount_in" type="text" name="" /></td>									
									<td><input class="code_in" type="text" name="" value="{{$t}}"/></td>									

								</tr>
								@else
								<tr>
									<td><input class="code_in" type="text" name="" value="" /></td>
									<td><input class="paticle_in" type="text" name="" value=""/></td>
									<td><input class="voucher_in" type="text" name="" value=""/></td>
									<td><input class="amount_in" type="text" name="" value="" /></td>
								</tr>								
								@endif
									
							</tbody>
						</table>
					</div>				
				</div>
			</div>			
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="single_footer">
					<h4>Accountant</h4>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="single_footer">
					<h4>Verified By</h4>
				</div>
			</div>	
			<div class="col-lg-4">
				<div class="single_footer">
					<h4>Approved By</h4>
				</div>
			</div>				
		</div>		
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>