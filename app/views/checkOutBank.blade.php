@extends('master')

@section('content')
@include('innerMenucommands')
<div class="showtable" id="responsbox">
  <div style="width:300px;margin:0 auto;"class="showInput">
    {{ Form::open(array('url' => 'checkOutBank', 'method' => 'post')) }}
    
    {{Form::label('fromdate','Enter Date(from):')}}
    {{Form::text('fromdate', null,array('class' => 'format datepicker form-control','id'=>'datepicker'))}}
    
    {{Form::label('todate','Enter Date(to):')}}
    {{Form::text('todate', null,array('class' => 'format datepicker form-control','id'=>'datepicker1'))}}
    {{Form::submit('Submit', array('class' => 'btn btn-primary'))}}
    {{ Form::close() }}
  </div>
<div class="mainContentBank">

  <h2>Account Info</h2>
  <p></p>            
  <table class="table table-bordered bank-remaining-balance">
    <thead>
      <tr>
      <?php if ($remaining_balance>0) {$rmclass='success';} else{$rmclass='warning';}  ?>
        <th class="<?php echo $rmclass;?>">Remaining Balance</th>
        <td colspan="2" class="<?php echo $rmclass;?>">{{ $remaining_balance }}</td>
      </tr>
    </thead>
    <tbody>
      <tr class="danger">
         <th >Last Withdraw</th>
        <td>{{ $last_withdraw_amount }}</td> 
       <td>{{ $last_withdraw_cdate }}</td> 
      </tr>
      <tr class="success">
         <th >Last Deposit</th>
        <td>{{ $last_deposit_amount }}</td>
        <td>{{ $last_deposit_cdate }}</td>

      </tr>
    </tbody>
  </table>
 
</div>

  @if (!empty($users))
<table id="paginationNps" class="table table-bordered table-hover">
  <h3 style="width:100%; text-align:center; margin:10px;">Transaction Report By Date:</h3>
  <hr>
    <thead>
      <tr class="danger">
      	<th>Transaction Date</th>
      	<th>Last Update Date</th>
        <th>Id No.</th>
        <th>Transaction Type</th>
        <th>Amount(in Taka)</th>
        <th>Voucher No.</th>
        <th>Additional Note/Purpose</th>
        <th>Transaction By(username)</th>
        <th class="norow">More</th>
      </tr>
    </thead>
    <tbody>
     <span style="display:none">{{$total=0;$tda=0;$twa=0;}}</span>
    	@foreach ($users as $property)
      
      <tr class="success">
        <td>{{ $property->created_at }}</td>
        <td>{{ $property->updated_at }}</td>
        <td>{{ $property->id }}</td>
        <td><?php if(($property->type)=='0') { echo "Withdraw"; $twa=$twa+$property->amount;} else{echo "Deposit";$tda=$tda+$property->amount;} ?></td>
        <td>{{ $property->amount }}</td>
        <td>{{ $property->voucher }}</td>
        <td>{{ $property->purpose }}</td>
        <td>{{ $property->username }}</td>
      <span style="display:none">{{$total=$tda-$twa;}}</span>
       	<td class="norow"><a href="{{ URL::to('details',$property->id) }}">View Details</a></td>
      </tr>
      @endforeach

    </tbody>
    <tfoot>
    	<tr class="warning">
        <td>	&nbsp;</td>
        <td>	&nbsp;</td>
        <td>	&nbsp;</td>
        <td class="succss">Remaining Amount Of The Range:</td>
        <td>{{ $total }}</td>
        <td>	&nbsp;</td>
        <td>	&nbsp;</td>
        <td>	&nbsp;</td>
        <td>	&nbsp;</td>
      </tr>
    </tfoot>
  </table>
<button class="pbtn" onclick="window.print();">Print Content</button>
<span style="border-top:1px solid; float:right;margin-top:40px;">Athaurized Signature</span>
  @else
  <br>
  <div class="alert alert-warning fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please Insert The Date Range.</strong></div>
  @endif
</div>
  </div>
@stop