@extends('master')

@section('content')
@include('rinnerMenucommands')
<div class="showtable" id="responsbox">
  <div style="width:300px;margin:0 auto;"class="showInput">
    {{ Form::open(array('url' => 'showmonthrev', 'method' => 'post')) }}
    {{Form::label('fromdate','Enter Date(from):')}}
    {{Form::text('fromdate', null,array('class' => 'form-control','id'=>'selectormonth'))}}
    {{Form::submit('Submit', array('class' => 'btn btn-primary'))}}
    {{ Form::close() }}
  </div>
  <style>
 .ui-datepicker-calendar{
            display: none;
        }
  </style>

  @if (!empty($users))
       <span style="display:none">{{$total=0;}}</span>
<table id="paginationNps" class="table table-bordered table-hover">
  <h3 style="width:100%; text-align:center; margin:10px;">Monthly Revenue Report</h3>
    <thead>
      <tr class="danger">
      	<th>Revenue Earning Date</th>
        <th>Amount(in Taka)</th>

      </tr>
    </thead>
    <tbody>
    	@foreach ($users as $property)
      <tr class="success">
         <span style="display:none">{{$total=$total+$property->amount;}}</span>
        <td>{{ $property->created_at }}</td>

        <td>{{ $property->amount }}</td>

      </tr>
      @endforeach
      </tbody>
      <tfoot>
        <tr class="warning">

        <td class="succss">Total Revenue Of The Range:</td>
        <td>{{ $total }}</td>
      </tr>
    </tfoot>
  </table>
  
<button class="pbtn" onclick="window.print();">Print Content</button>
<span style="border-top:1px solid; float:right;margin-top:40px;">Athaurized Signature</span>
  @else
  <br>
  <div class="alert alert-warning fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please Insert The Date Range.</strong></div>
  @endif
</div>
@stop