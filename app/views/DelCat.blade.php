@extends('master')

@section('content')
@include('innerMenucommands')
<script type="text/javascript">

    function do_this(){

        var checkboxes = document.getElementsByName('ditem[]');
        var button = document.getElementById('toggle');

        if(button.value == 'select'){
            for (var i in checkboxes){
                checkboxes[i].checked = 'FALSE';
            }
            button.value = 'deselect'
        }else{
            for (var i in checkboxes){
                checkboxes[i].checked = '';
            }
            button.value = 'select';
        }
    }
</script>
<div class="showtable" id="responsbox">
  <div style="width:300px;margin:0 auto;"class="showInput">
        <p>Select a category:</p>
                {{Form::open(array('url' => 'DelCat', 'method' => 'post')) }}
              {{Form::label('fromdate','Enter Category Type:')}}
              {{ Form::select('ctype', array(
                                  'Slect A Type',
                                  'revenue'=>'Revenue Area',
                                  'expenditure'=>'Expenditure Area'
                                  ),'2',['class' => 'btn']) }}
                                 </br>
              {{Form::submit('Submit', array('class' => 'btn btn-primary btn-action-u'))}}
              {{ Form::close() }}
  </div>

  @if (!empty($users))
         <span style="display:none">{{$total=0;}}</span>
<table id="paginationNps" class="table table-bordered table-hover">
    <h3 style="width:100%; text-align:center; margin:10px;">Cost Report By Year:</h3>
    <thead>
      <tr class="danger">
<th>CheckBox</th>
        <th>Sl No.</th>

      	<th>Category Name</th>

        <th>Category ID</th>
        <th>Category Type</th>
        <th>Action</th>

      </tr>
    </thead>
    <tbody>
{{Form::open(array('url' => 'deleteCat', 'method' => 'post')) }}
    	@foreach ($users as $property)
               <span style="display:none"></span>
      <tr class="success">


<td>{{ Form::checkbox('ditem[]',$property->id)}}</td>
        <td>{{ $property->id }}</td>
<td>{{ $property->cname }}</td>
<td>{{ $property->c_id }}</td>
        <td>{{ $property->ctype}}</td>
<td><button class="btn btn-primary" type="button" data-toggle="modal" data-target="#my{{ $property->id }}">Edite</button></td>

      </tr>

      @endforeach
<input style="float: right; width: 100px;height:50px" class="btn-success"type="button" id="toggle" value="select" onClick="do_this()" />
<input style="float: right; width: 200px;height:50px !important;overflow:hidden;padding: 0;" class="btn-warning"type="submit" name="submit" value="Delete Selected Item"><br>
              {{ Form::close() }} 

    </tbody>
  </table>

<button class="pbtn" onclick="window.print();">Print Content</button>
<span style="border-top:1px solid; float:right;margin-top:40px;">Athaurized Signature</span>
  @else
  <br>
  <div class="alert alert-warning fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please Insert The Date Range.</strong></div>
  @endif
</div>
@stop
