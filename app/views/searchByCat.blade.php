@extends('master')

@section('content')
@include('innerMenucommands')

<div class="showtable" id="responsbox">
  <div style="width:300px;margin:0 auto;"class="showInput">
    {{ Form::open(array('url' => 'searchByCat', 'method' => 'post')) }}
    {{Form::label('type','Cost Particularies:')}}
    @if (!empty($categories))
    <span class="inputcostSelect form-control">

    {{Form::select('type', (['0' => 'Select a Category'] + $categories), null )}}

    </span>
    @endif
    <br>
    {{Form::label('fromdate','Enter Date(from):')}}
    {{Form::text('fromdate', null,array('class' => 'format datepicker form-control','id'=>'datepicker'))}}
    
    {{Form::label('todate','Enter Date(to):')}}
    {{Form::text('todate', null,array('class' => 'format datepicker form-control','id'=>'datepicker1'))}}
    {{Form::submit('Submit', array('class' => 'btn btn-primary'))}}
    {{ Form::close() }}
  </div>

  @if (!empty($users))
<table id="paginationNps" class="table table-bordered table-hover">
  <h3 style="width:100%; text-align:center; margin:10px;">Cost Report By Date:</h3>
  <hr>
    <thead>
      <tr class="danger">
        <th>Cost Date</th>
        <th>Last Update Date</th>
        <th>Id No.</th>
        <th>Particularies of Cost</th>
        <th>Amount(in Taka)</th>
        <th>Voucher No.</th>
        <th>Additional Note</th>
        <th class="norow">More</th>
      </tr>
    </thead>
    <tbody>
     <span style="display:none">{{$total=0;}}</span>
      @foreach ($users as $property)
      
      <tr class="success">
        <td>{{ $property->created_at }}</td>
        <td>{{ $property->updated_at }}</td>
        <td>{{ $property->id }}</td>
        <td>{{ $property->type }}</td>
        <td>{{ $property->amount }}</td>
        <td>{{ $property->voucher }}</td>
        <td>{{ $property->note }}</td>
      <span style="display:none">{{$total=$total+$property->amount;}}</span>
        <td class="norow"><a href="{{ URL::to('details',$property->id) }}">View Details</a></td>
      </tr>
      @endforeach
      <tr class="warning">
        <td></td>
        <td></td>
        <td></td>
        <td class="succss">Total cost Of The Range:</td>
        <td>{{ $total }}</td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </tbody>
  </table>
<button class="pbtn" onclick="window.print();">Print Content</button>
<span style="border-top:1px solid; float:right;margin-top:40px;">Athaurized Signature</span>
  @else
  <br>
  <div class="alert alert-warning fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please Insert The Date Range.</strong></div>
  @endif
</div>
@stop