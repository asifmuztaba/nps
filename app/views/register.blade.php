@extends('master')

@section('content')
<div class="login_form">
						<h2>North Point Payment App</h2>
							<span href="#" class="button" id="toggle-login">Admin</span>

							<div id="login">
							  <div id="triangle"></div>
							  <h1>Register</h1>
{{ Form::open(array('url' => 'reginsert', 'method' => 'post')) }}

{{Form::label('username','Username')}}
{{Form::text('username', null,array('class' => 'form-control'))}}

{{Form::label('password','Password')}}
{{Form::password('password',array('class' => 'form-control'))}}

{{Form::label('name','Full Name')}}
{{Form::text('name', null,array('class' => 'form-control'))}}

{{Form::label('phone','Phone No.')}}
{{Form::text('phone', null,array('class' => 'form-control'))}}

{{Form::submit('Register', array('class' => 'btn btn-primary'))}}

{{ Form::close() }}


							</div>
						</div>

@stop

