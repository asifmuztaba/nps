@extends('master')

@section('content')
@include('innerMenucommands')
<div class="mainContentBank">

  <h2>Account Info</h2>
  <p></p>            
  <table class="table table-bordered bank-remaining-balance">
    <thead>
      <tr>
      <?php if ($remaining_balance>0) {$rmclass='success';} else{$rmclass='warning';}  ?>
        <th class="<?php echo $rmclass;?>">Remaining Balance</th>
        <td colspan="2" class="<?php echo $rmclass;?>">{{ $remaining_balance }}</td>
      </tr>
    </thead>
    <tbody>
      <tr class="danger">
         <th >Last Withdraw</th>
        <td>{{ $last_withdraw_amount }}</td> 
       <td>{{ $last_withdraw_cdate }}</td> 
      </tr>
      <tr class="success">
         <th >Last Deposit</th>
        <td>{{ $last_deposit_amount }}</td>
        <td>{{ $last_deposit_cdate }}</td>

      </tr>
    </tbody>
  </table>
 
</div>
<div class="account_sce">
              <a href="{{ URL::to('withdrawBank') }}"><button type="button">Transection</button></a>
              <a href="{{ URL::to('checkOutBank') }}"><button type="button">Check Out Report</button></a>
             
            </div>
@stop
