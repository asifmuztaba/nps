						<div class="innermenu2">
							<ul>
								<li><a href="{{ URL::to('home') }}">Home</a></li>
								<li><a href="{{ URL::to('accountManagement') }}">Account Section</a></li>
								<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" >
                View
                <span class="caret"></span>
              </a>
								    
									<ul class="dropdown-menu">	
										<li><a href="{{ URL::to('show') }}">View By Date</a></li>
										<li><a href="{{ URL::to('searchByCat') }}">View By Category</a></li>
										<li><a href="{{ URL::to('showmonth') }}">View by Month</a></li>
										<li><a href="{{ URL::to('viewByYear') }}">View by Year</a></li>
										<li><a href="{{ URL::to('viewReport') }}">View Report</a></li>
									</ul>
								</li>
								<li><a href="{{ URL::to('logout') }}">Logout</a></li>
							</ul>
						</div>