@extends('master')

@section('content')
    

<div class="login_form">
						<h2>North Point Payment App</h2>
							<span href="#" class="button" id="toggle-login">Admin</span>

							<div id="login">
							  <div id="triangle"></div>
							  <h1>Log in</h1>
{{ Form::open(array('url' => 'userlogin', 'method' => 'post')) }}
{{Form::label('username','Username')}}
{{Form::text('username', null,array('class' => 'form-control'))}}
{{Form::label('password','Password')}}
{{Form::password('password',array('class' => 'form-control'))}}
{{Form::submit('Login', array('class' => 'btn btn-primary'))}}
{{ Form::close() }}
<div style="padding-top:10px; width:100%;">
<a style="width:100%;"class="btn btn-primary"href="{{ url('register')}}">New User? Please Register</a>
</div>
							</div>
						</div>
@stop

