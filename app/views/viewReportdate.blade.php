@extends('master')

@section('content')
@include('innerMenucommands')
<div class="showtable" id="responsbox">
  <div class="container">
    <div class="row">
      <div class="col-md-6">


  <div style="width:300px;margin:0 auto;"class="showInput">
    {{ Form::open(array('url' => 'viewReportdate', 'method' => 'post')) }}
    {{Form::label('fromdate','Enter Date(from):')}}
   {{Form::text('fromdate', null,array('class' => 'format datepicker form-control','id'=>'datepicker'))}}
    {{Form::submit('Submit', array('class' => 'btn btn-primary'))}}
    {{ Form::close() }}
  </div>
  </div>
  <div class="col-md-6" style="text-align:center">
    <a href="{{ URL::to('searchByCat') }}"><button class="btn btn-success">Check By Catagory</button></a>
  </div>
    </div>

  </div>
  <style>

  </style>

  @if (!empty($users))
       <span style="display:none">
        {{$total=0;}}
        {{$total1=0;}}
        {{$s=0;}}
        {{$l=0;}}
        {{$dif=0;}}
{{$o=0;}}
      @foreach ($users as $property )
      {{$total=$total+$property->amount;}}

@endforeach
@foreach ($users1 as $p)
      {{$total1=$total1+$p->amount;}}
      @endforeach
      {{ $dif=$total1-$total;}}
@if ($dif < 0)
{{$s=0}}
{{$o=0-$dif}}
{{$dif=0}}
@endif

       </span>
<table id="paginationNps" class="table table-bordered table-hover">
  <h3 style="width:100%; text-align:center; margin:10px;">Cost Report For Date:<span style="color:red;">{{$property->created_at}}</span></h3>
    <thead>
      <tr class="danger">
      	<th>Total Income</th>

        <th>Total Expenditure</th>
        <th>Profit</th>
        <th>Loss</th>
      </tr>
    </thead>
    <tbody>

      <tr class="success">
         <span style="display:none"></span>
        <td>{{ $total1}}</td>


        <td>{{ $total}}</td>
        <td>{{ $dif }}</td>
        <td>{{ $o }}</td>

      </tr>
      </tbody>
      <tfoot>
        <tr style="display:none" class="warning">

        <td class="succss">Total cost Of The Range:</td>
        <td>{{ $total }}</td>
        <td></td>
        <td></td>
      </tr>
    </tfoot>
    <button class="pbtn" onclick="window.print();">Print Content</button>
  </table>
  <span style="border-top:1px solid; float:right;margin-top:40px;">Athaurized Signature</span>
  @else
  <br>
  <div class="alert alert-warning fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please Insert The Date Range.</strong></div>
  @endif
  
</div>

@stop