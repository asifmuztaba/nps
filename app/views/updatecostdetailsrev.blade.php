@extends('master')

@section('content')
@include('rinnerMenucommands')
<div class="inputcost login_form" id="login">
{{ Form::open(array('url' => ['updatecostmrev',$users->id])) }}
{{Form::label('type','Cost Particularies:')}}
    @if (!empty($categories))
    <span class="inputcostSelect form-control">

    {{Form::select('type', (['0' => 'Select a Category'] + $categories), null )}}

    </span>
    @endif
<br>
{{Form::label('amount','Amount In Taka:')}}
{{Form::text('amount', $users->amount,array('class' => 'form-control'))}}

{{Form::label('voucher','Enter Voucher or Check No:')}}
{{Form::text('voucher',$users->voucher,array('class' => 'form-control'))}}

{{Form::label('note','Enter Additional Notes:')}}
{{Form::textarea('note', $users->note, ['class' => 'form-control']) }}
<br>
{{Form::submit('Update Cost', array('class' => 'btn btn-primary'))}}
{{ Form::close() }}

</div>

@stop