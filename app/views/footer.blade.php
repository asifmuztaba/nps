</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="footer">
						<p>&copy; All right reserved by NorthPoint School.</p>
						<p>Design & Developed by <a href="http://www.cloudtechbd.com" target="_blank">Cloud Technolog BD</a></p>
					</div>
				</div>
			</div>			
		</div>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
		<script src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
		<script>
		$( ".datepicker" ).datepicker();
			$( ".format" ).change(function() {
			$( ".datepicker" ).datepicker( "option", "dateFormat","yy-mm-dd" );

		});
	$(document).ready(function() {
        $("#selectormonth").datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'MM yy',
                onClose: function(dateText, inst) {
                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    $(this).datepicker('setDate', new Date(year, month, 1));
                }
 
            });
    });

	$(document).ready(function() {
    $('#selectoryear').datepicker({
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yy',
        onClose: function(dateText, inst) { 
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, 1));
        }
    });
 $(".date-picker-year").focus(function () {
        $(".ui-datepicker-month").hide();
    });
});
 
		</script>
		<script type="text/javascript">
			$('#toggle-login').click(function(){
			  $('#login').toggle();
			});		
		</script>
		<script type="text/javascript">
			jQuery(document).ready(function() {
			    jQuery('#paginationNps').DataTable();
			} );
		</script>
	</body>
</html>