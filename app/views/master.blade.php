@include('header')

@if(Session::has('message'))
<div class="alert alert-danger fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong></strong>{{ Session::get('message') }}</div>
@endif
@yield('content')
					

@include('footer');
